/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package controller;

import impl.DaoImplEmpleado;
import java.io.IOException;
import java.util.List;
import javax.swing.table.DefaultTableModel;
import pojo.Empleado;

/**
 *
 * @author Jadpa21
 */
public class IFrmCatalogoEmpleadoController {
    private DaoImplEmpleado daoEmpleado;
    private String header[] = {"Id","Cedula","INSS","Nombres",
        "Apellidos","Salario"};

    public IFrmCatalogoEmpleadoController() {
        daoEmpleado = new DaoImplEmpleado();
    }
    
    public DefaultTableModel getTableModel() throws IOException{
        List<Empleado> empleados = daoEmpleado.getAll();
        Object[][] data = new Object[empleados.size()][header.length];
        int i = 0;
        for(Empleado e : empleados){
            data[i++] = getData(e);
        }
        
        return new DefaultTableModel(data, header);
    }
    
    public Object[] getData(Empleado e){
        Object[] singleData = new Object[header.length];
        singleData[0] = e.getId();
        singleData[1] = e.getCedula();
        singleData[2] = e.getInss();
        singleData[3] = e.getNombres();
        singleData[4] = e.getApellidos();
        singleData[5] = e.getSalario();
        
        return singleData;
    }
    
    
}
